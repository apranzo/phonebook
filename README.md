# Phonebook 

** Functionality: **

+ login/logout/remember me
+ ajax search of employees 
+ display profile employee 
+ edit profile employee 
+ add/delete profile
+ upload and download avatar

  
** Screenshots: **


Login:


![81275d937e3cbcecf47b671e6eacc50e.png](https://bitbucket.org/repo/RL4Rdj/images/1366940119-81275d937e3cbcecf47b671e6eacc50e.png)


Search page:


![](https://i.gyazo.com/4fa9383248d9707b1142b181574ce25f.png)



View employee: 

![](https://i.gyazo.com/aba8bae5fefa943adde1979ad1b05630.png)



Edit employee: 

![](https://i.gyazo.com/3e5b29775d8d303ca26f991e3342b678.png)



Ajax search of employees: 

![](https://i.gyazo.com/a55fcf88ab5d17da2f5d9c2e5f8bce80.png)







** Tools: **  
JDK 7, Spring 4, JPA/ Hibernate 5, jQuery 1.12, Twitter Bootstrap 3.3.6, JUnit 4.11, Mockito, slf4j/log4j2, Maven 3, Git / Bitbucket, Tomcat 8, MySql / H2, IntelliJIDEA 14.  


** Notes: **  
SQL ddl is located in the `db/ddl.sql`


--  
**Подобед Александр**

Тренинг getJavaJob,
[http://www.getjavajob.com](http://www.getjavajob.com)