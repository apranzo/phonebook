package com.getjavajob.phonebook.dao;

import com.getjavajob.phonebook.dao.imp.EmployeeDAO;
import com.getjavajob.phonebook.model.Department;
import com.getjavajob.phonebook.model.Employee;
import com.getjavajob.phonebook.model.Phone;
import com.getjavajob.phonebook.model.Type;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-test-context.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
public class EmployeeDaoTest {
    private static Employee employee;
    private static Department department;
    private static Phone phone;
    @Autowired
    private EmployeeDAO employeeDAO;

    @Before
    public void init() throws IOException, SQLException {
        Set<Phone> phoneList = new HashSet<>();
        phone = new Phone("1111", Type.WORK);
        phoneList.add(phone);
        department = new Department("dep", phone);
        employee = new Employee("name", "family", "mail", "icq", "skype", department,
                new Employee("name2", "family2"), phoneList, "addressW", "addressH", new Date(11, 2, 9));
    }

    @Test
    public void getAll() {
        Set<Employee> phoneList2 = new HashSet<>();
        Employee a = new Employee("a", "1");
        Employee b = new Employee("b", "2");
        Employee c = new Employee("c", "3");
        Employee d = new Employee("d", "4");
        employeeDAO.add(a);
        employeeDAO.add(b);
        employeeDAO.add(c);
        employeeDAO.add(d);
        employeeDAO.add(employee);
        phoneList2.add(a);
        phoneList2.add(b);
        phoneList2.add(c);
        phoneList2.add(d);
        phoneList2.add(employee);
        phoneList2.add(employee.getChief());
        Set<Employee> all = employeeDAO.getAll();
        Assert.assertEquals(phoneList2, all);
    }

    @Test
    public void get() {
        Employee employee2 = employeeDAO.add(employee);
        Assert.assertEquals(employeeDAO.get(employee2.getId()), employee);
    }

    @Test
    public void insert() {
        employeeDAO.add(employee);
        assertEquals(2, employeeDAO.getAll().size());
    }

    @Test
    public void delete() {
        Employee employee2 = employeeDAO.add(employee);
        employeeDAO.delete(employee2.getId());
        assertEquals(null, employeeDAO.get(employee2.getId()));
    }

    @Test
    public void update() {
        Set<Phone> ll = new HashSet<>();
        ll.add(new Phone("test", Type.WORK));
        employee.setPhones(ll);
        employee.setName("newName");
        employeeDAO.update(employee);
        Employee employee = employeeDAO.get(1);
        assertEquals(employee, employeeDAO.get(employee.getId()));
    }
}
