package com.getjavajob.phonebook.dao;

import com.getjavajob.phonebook.dao.imp.DepartmentDAO;
import com.getjavajob.phonebook.dao.imp.EmployeeDAO;
import com.getjavajob.phonebook.model.Department;
import com.getjavajob.phonebook.model.Employee;
import com.getjavajob.phonebook.model.Phone;
import com.getjavajob.phonebook.model.Type;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by podobeda on 25.04.2016 22:34
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-test-context.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
public class DepartmentDaoTest {
    private static Employee employee;
    private static Department department;
    private static Phone phone;
    @Autowired
    private EmployeeDAO employeeDAO;
    @Autowired
    private DepartmentDAO departmentDAO;

    @Before
    public void init() throws IOException, SQLException {
        Set<Phone> phoneList = new HashSet<>();
        phone = new Phone("1111", Type.WORK);
        phoneList.add(phone);
        department = new Department("dep", phone);
        employee = new Employee("name", "family", "mail", "icq", "skype", department,
                new Employee("name2", "family2"), phoneList, "addressW", "addressH", new Date(11, 2, 9));
    }

    @Test
    public void getAll() {
        Set<Department> departmentSet = new HashSet<>();
        Department a = new Department("a");
        Department b = new Department("b");
        Department c = new Department("c");
        Department d = new Department("d");
        d.setHead(employeeDAO.add(employee));
        departmentDAO.add(a);
        departmentDAO.add(b);
        departmentDAO.add(c);
        departmentDAO.add(d);
        departmentSet.add(a);
        departmentSet.add(b);
        departmentSet.add(c);
        departmentSet.add(d);
        departmentSet.add(department);
        Set<Department> all = departmentDAO.getAll();
        Assert.assertEquals(departmentSet, all);
    }

    @Test
    public void get() {
        Department department1 = departmentDAO.add(department);
        Assert.assertEquals(departmentDAO.get(department1.getId()), department);
    }

    @Test
    public void insert() {
        departmentDAO.add(department);
        assertEquals(1, departmentDAO.getAll().size());
    }

    @Test
    public void delete() {
        Employee emp = (employeeDAO.add(employee));
        Department department1 = emp.getDep();
        emp.setDep(department1);
        departmentDAO.delete(department1.getId());
        assertEquals(null, departmentDAO.get(department1.getId()));
    }

    @Test
    public void update() {
        Department department1 = departmentDAO.add(department);
        Phone ll = new Phone("test", Type.WORK);
        department1.setPhone(ll);
        department1.setName("newName");
        departmentDAO.update(department1);
        assertEquals(department1, departmentDAO.get(department1.getId()));
    }
}
