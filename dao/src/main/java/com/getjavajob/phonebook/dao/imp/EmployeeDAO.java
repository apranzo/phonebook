package com.getjavajob.phonebook.dao.imp;

import com.getjavajob.phonebook.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO extends AbstractDAO<Employee> {

    @Autowired
    private DepartmentDAO departmentDAO;
    @Autowired

    @Override
    public Class<Employee> getMyClass() {
        return Employee.class;
    }
}
