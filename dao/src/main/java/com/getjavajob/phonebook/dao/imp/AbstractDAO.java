package com.getjavajob.phonebook.dao.imp;

import com.getjavajob.phonebook.dao.Crud;
import com.getjavajob.phonebook.model.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.HashSet;
import java.util.Set;

@Repository
public abstract class AbstractDAO<T extends BaseEntity> implements Crud<T> {

    @Autowired
    private EntityManager em;

    @Override
    public T add(T entity) {
        return em.merge(entity);
    }


    @Override
    public T update(T entity) {
        return em.merge(entity);
    }


    @Override
    public void delete(int id) {
        em.remove(em.find(getMyClass(), id));
    }

    @Override
    public T get(int id) {
        return em.find(getMyClass(), id);
    }

    public abstract Class<T> getMyClass();

    public Set<T> getAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getMyClass());
        CriteriaQuery<T> select = criteriaQuery.select(criteriaQuery.from(getMyClass()));
        return new HashSet<>(em.createQuery(select).getResultList());
    }
}
