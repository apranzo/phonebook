package com.getjavajob.phonebook.dao.imp;

import com.getjavajob.phonebook.model.Phone;
import org.springframework.stereotype.Repository;

@Repository
public class PhoneDAO extends AbstractDAO<Phone> {

    @Override
    public Class<Phone> getMyClass() {
        return Phone.class;
    }
}
