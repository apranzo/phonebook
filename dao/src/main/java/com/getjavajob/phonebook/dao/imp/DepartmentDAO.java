package com.getjavajob.phonebook.dao.imp;

import com.getjavajob.phonebook.model.Department;
import org.springframework.stereotype.Repository;

@Repository
public class DepartmentDAO extends AbstractDAO<Department> {

    @Override
    public Class<Department> getMyClass() {
        return Department.class;
    }
}
