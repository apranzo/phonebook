package com.getjavajob.phonebook.dao;

import com.getjavajob.phonebook.model.BaseEntity;

import java.util.Set;

public interface Crud<T extends BaseEntity> {

    T add(T entity);

    T update(T entity);

    void delete(int id);

    T get(int id);

    Set<T> getAll();
}
