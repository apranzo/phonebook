package com.getjavajob.phonebook.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by podobeda on 11.12.2015 2:16
 */
@Entity
public class Department extends BaseEntity implements Serializable {
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_head")
    private Employee head;
    private String name;
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Phone phone;


    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public Department(String name, Phone phone) {
        this(name);
        this.phone = phone;
    }

    public Department(String name, Employee head, Phone phone) {
        this(name);
        this.head = head;
        this.phone = phone;
    }

    public Employee getHead() {
        return head;
    }

    public void setHead(Employee head) {
        this.head = head;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, name, phone);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Department dep = (Department) obj;
        return (Objects.equals(dep.head, head) &&
                Objects.equals(dep.name, name) &&
                Objects.equals(dep.phone, phone));
    }
}
