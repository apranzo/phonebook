package com.getjavajob.phonebook.model;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by podobeda on 11.12.2015 2:19
 */
@Entity
public class Phone extends BaseEntity implements Serializable {

    private String number;
    private Type type;

    public Phone() {
    }


    public Phone(String number) {
        this.number = number;
    }

    public Phone(String number, Type type) {
        this(number);
        this.type = type;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Phone ph = (Phone) obj;
        return (Objects.equals(ph.number, number) && Objects.equals(ph.type, type));
    }

    @Override
    public String toString() {
        return number + "_" + type;
    }
}
