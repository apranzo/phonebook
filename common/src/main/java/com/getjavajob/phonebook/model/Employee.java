package com.getjavajob.phonebook.model;

import com.getjavajob.phonebook.model.converters.ImageConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * Created by podobeda on 11.12.2015 1:39
 */

@Entity
@Table(name = "employees")
public class Employee extends BaseEntity implements Serializable {
    private String name;
    private String family;
    private String mail;
    private String icq;
    private String skype;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_dep")
    private Department dep;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_chief")
    private Employee chief;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "employees_has_phone", joinColumns = @JoinColumn(name = "employees_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_id"))
    private Set<Phone> phones;
    @Column(name = "addrsW")
    private String addrsW;
    @Column(name = "addrsH")
    private String addrsH;
    @Column(name = "birthday", columnDefinition = "VARCHAR(40)")
    private Date birth;

    @Column(name = "photo", columnDefinition = "MEDIUMBLOB")
    @Convert(converter = ImageConverter.class)
    private String photo;

    public Employee() {
    }

    public Employee(String name, String family) {
        this.name = name;
        this.family = family;
    }

    public Employee(String name, String family, String mail, String icq, String skype,
                    Department dep, Employee chief, Set<Phone> phones, String addrsH, String addrsW, Date birth) {
        this.name = name;
        this.family = family;
        this.mail = mail;
        this.icq = icq;
        this.skype = skype;
        this.dep = dep;
        this.chief = chief;
        this.phones = phones;
        this.addrsW = addrsW;
        this.addrsH = addrsH;
        this.birth = birth;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddrsW() {
        return addrsW;
    }

    public void setAddrsW(String addrsW) {
        this.addrsW = addrsW;
    }

    public String getAddrsH() {
        return addrsH;
    }

    public void setAddrsH(String addrsH) {
        this.addrsH = addrsH;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Department getDep() {
        return dep;
    }

    public void setDep(Department dep) {
        this.dep = dep;
    }

    public Employee getChief() {
        return chief;
    }

    public void setChief(Employee chief) {
        this.chief = chief;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != obj.getClass()) return false;
        Employee emp = (Employee) obj;

        return (Objects.equals(name, emp.name) &&
                Objects.equals(family, emp.family) &&
                Objects.equals(emp.icq, icq) &&
                Objects.equals(emp.skype, skype) &&
                Objects.equals(emp.dep, dep) &&
                Objects.equals(emp.chief, chief) &&
                Objects.equals(emp.phones, phones) &&
                Objects.equals(emp.addrsW, addrsW) &&
                Objects.equals(emp.addrsH, addrsH) &&
                Objects.equals(emp.birth, birth));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, family, skype, icq, mail, dep, chief, phones, addrsW, addrsH, birth);
    }
}
