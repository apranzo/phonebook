package com.getjavajob.phonebook.model.converters;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by podobeda on 03.05.2016 19:06
 */
@Converter
public class ImageConverter implements AttributeConverter<String, byte[]> {
    @Override
    public byte[] convertToDatabaseColumn(String s) {
        if (s != null) {
            return s.getBytes();
        }
        return null;
    }

    @Override
    public String convertToEntityAttribute(byte[] bytes) {
        if (bytes != null) {
            return new String(bytes);
        }
        return null;
    }
}
