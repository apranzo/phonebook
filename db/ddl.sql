DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `id_head` INT(11) NULL DEFAULT NULL,
  `phone_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE (`name`)
);

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `family` varchar(45) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `birthday` VARCHAR(41) NULL DEFAULT NULL,
  `icq` varchar(10) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `id_dep` int(11) NULL DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `id_chief` int(11) DEFAULT NULL,
  `photo` MEDIUMBLOB NULL DEFAULT NULL,
  `addrsH` VARCHAR(255) NULL DEFAULT NULL,
  `addrsW` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `employees_has_phone`;
CREATE TABLE `employees_has_phone` (
  `employees_id` INT(11) NOT NULL,
  `phone_id` INT(11) NOT NULL,
  PRIMARY KEY (`employees_id`,`phone_id`)
);

DROP TABLE IF EXISTS `phone`;
CREATE TABLE `phone` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(255) NULL DEFAULT NULL,
  `type` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `employees` add constraint  Eid_chief2Eid FOREIGN KEY (`id_chief`) REFERENCES `employees` (`id`) ON DELETE SET NULL;
ALTER TABLE `employees_has_phone` add constraint  EHPemployees_id2Eid FOREIGN KEY (`employees_id`) REFERENCES `employees` (`id`);
ALTER TABLE `employees_has_phone` add constraint   EHPphone_id2Pid FOREIGN KEY (`phone_id`) REFERENCES `phone` (`id`);
ALTER TABLE `department` add constraint  Dphone_id2Pid FOREIGN KEY (`phone_id`) REFERENCES `phone` (`id`)ON DELETE CASCADE;
ALTER TABLE `employees` add constraint  Eid_dep2Did FOREIGN KEY (`id_dep`) REFERENCES `department` (`id`) ON DELETE SET NULL;
ALTER TABLE `department` add constraint  Did_head2Eid FOREIGN KEY (`id_head`) REFERENCES `employees` (`id`) ON DELETE SET NULL;
