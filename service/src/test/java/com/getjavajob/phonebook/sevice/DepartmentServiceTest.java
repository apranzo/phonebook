package com.getjavajob.phonebook.sevice;

import com.getjavajob.phonebook.dao.imp.DepartmentDAO;
import com.getjavajob.phonebook.model.Department;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentServiceTest {
    private DepartmentDAO departmentDAO;
    private DepartmentService departmentService;

    @Before
    public void init() {
        departmentDAO = mock(DepartmentDAO.class);
        departmentService = new DepartmentService(departmentDAO);
    }

    @Test
    public void getAll() {
        Set<Department> departments = makeDepartments();
        when(departmentDAO.getAll()).thenReturn(departments);
        Set<Department> departmentsResult = departmentService.getAll();
        assertEquals(4, departmentsResult.size());
    }

    @Test
    public void getFromId() {
        Department department2 = new Department();
        department2.setName("dep2");
        department2.setId(2);
        when(departmentDAO.get(2)).thenReturn(department2);
        assertEquals("dep2", departmentService.getFromId(2).getName());
    }

    @Test
    public void delete() {
        Set<Department> departments = makeDepartments();
        Department e = new Department("e");
        departments.add(e);
        when(departmentDAO.getAll()).thenReturn(departments);
        Set<Department> departmentsResult = departmentService.getAll();
        assertEquals(5, departmentsResult.size());
        departmentService.delete(1);
        departments.remove(e);
        departmentsResult = departmentService.getAll();
        assertEquals(4, departmentsResult.size());
    }

    @Test
    public void add() {
        Set<Department> departments = makeDepartments();
        when(departmentDAO.getAll()).thenReturn(departments);
        departments.add(new Department("e"));
        departments = departmentService.getAll();
        assertEquals(5, departments.size());
    }

    private Set<Department> makeDepartments() {
        HashSet<Department> departments = new HashSet<>();
        Department a = new Department("a");
        Department b = new Department("b");
        Department c = new Department("c");
        Department d = new Department("d");
        departments.add(a);
        departments.add(b);
        departments.add(c);
        departments.add(d);
        return departments;
    }
}
