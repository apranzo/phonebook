package com.getjavajob.phonebook.sevice;

import com.getjavajob.phonebook.dao.imp.EmployeeDAO;
import com.getjavajob.phonebook.model.Department;
import com.getjavajob.phonebook.model.Employee;
import com.getjavajob.phonebook.model.Phone;
import com.getjavajob.phonebook.model.Type;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
    private EmployeeDAO employeeDAO;
    private EmployeeService employeeService;

    @Before
    public void init() {
        employeeDAO = mock(EmployeeDAO.class);
        employeeService = new EmployeeService(employeeDAO);
    }

    @Test
    public void getAll() {
        Set<Employee> employees = makeEmployees();
        when(employeeDAO.getAll()).thenReturn(employees);
        Set<Employee> employeesResult = employeeService.getAll();
        assertEquals(2, employeesResult.size());
    }

    @Test
    public void getFromId() {
        Employee employee2 = new Employee();
        employee2.setName("emp2");
        employee2.setId(2);
        when(employeeDAO.get(2)).thenReturn(employee2);
        assertEquals("emp2", employeeService.getFromId(2).getName());
    }

    @Test
    public void delete() {
        Set<Employee> employees = makeEmployees();
        Employee e = new Employee("name", "family");
        employees.add(e);
        when(employeeDAO.getAll()).thenReturn(employees);
        Set<Employee> employeesResult = employeeService.getAll();
        assertEquals(3, employeesResult.size());
        employeeService.delete(1);
        employees.remove(e);
        employeesResult = employeeService.getAll();
        assertEquals(2, employeesResult.size());
    }

    @Test
    public void add() {
        Set<Employee> employees = makeEmployees();
        when(employeeDAO.getAll()).thenReturn(employees);
        employees.add(new Employee("name", "family"));
        employees = employeeService.getAll();
        assertEquals(3, employees.size());
    }

    private Set<Employee> makeEmployees() {
        Set<Phone> phoneList = new HashSet<>();
        Phone phone = new Phone("1111", Type.WORK);
        phoneList.add(phone);
        Department department = new Department("dep", phone);
        Set<Employee> employees = new HashSet<>();
        Employee employee2 = new Employee("name2", "family2", "mail2", "icq2", "skype2", null, null, null, "addressW2", "addressH2", null);
        employees.add(new Employee("name", "family", "mail", "icq", "skype", department,
                employee2, phoneList, "addressW", "addressH", new Date(11, 2, 9)));
        employees.add(employee2);
        return employees;
    }
}
