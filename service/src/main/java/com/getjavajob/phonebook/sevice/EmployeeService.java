package com.getjavajob.phonebook.sevice;

import com.getjavajob.phonebook.dao.imp.EmployeeDAO;
import com.getjavajob.phonebook.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Set;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeDAO dao;

    public EmployeeService() {
    }

    public EmployeeService(EmployeeDAO dao) {
        this.dao = dao;
    }

    public Set<Employee> getAll() {
        return dao.getAll();
    }

    @Transactional
    public void add(Employee emp) {
        dao.add(emp);
    }

    @Transactional
    public Employee getFromId(int id) {
        return dao.get(id);
    }

    @Transactional
    public void delete(int id) {
        dao.delete(id);
    }

    public String getSimpleBirth(Employee emp) {
        if (emp.getBirth() == null) {
            return "unknown";
        }
        return new SimpleDateFormat("dd.MM.yyyy").format(emp.getBirth());
    }
}
