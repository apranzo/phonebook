package com.getjavajob.phonebook.sevice;

import com.getjavajob.phonebook.dao.imp.DepartmentDAO;
import com.getjavajob.phonebook.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentDAO dao;

    public DepartmentService() {
    }

    public DepartmentService(DepartmentDAO dao) {
        this.dao = dao;
    }

    @Transactional
    public Set<Department> getAll() {
        return dao.getAll();
    }

    @Transactional
    public Department getFromId(int id) {
        return dao.get(id);
    }

    @Transactional
    public void addOrUpdate(Department department) {
        dao.add(department);
    }

    @Transactional
    public void delete(int id) {
        dao.delete(id);
    }
}
