package com.getjavajob.phonebook.sevice;

import com.getjavajob.phonebook.dao.imp.PhoneDAO;
import com.getjavajob.phonebook.model.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PhoneService {

    @Autowired
    private PhoneDAO dao;

    public Set<Phone> getAll() {
        return dao.getAll();
    }
}
