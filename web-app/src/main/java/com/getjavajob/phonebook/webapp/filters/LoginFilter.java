package com.getjavajob.phonebook.webapp.filters;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Properties;

public class LoginFilter implements Filter {
    private String[] locked;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Properties props = new Properties();
        try {
            props.load(this.getClass().getClassLoader().getResourceAsStream("lockedPages.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        locked = props.getProperty("locked").split(";");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        if (isLoked(req.getRequestURI()) && (session == null || session.getAttribute("logged") == null && !cookieCheck(req))) {
            res.sendRedirect("/");
        } else {
            chain.doFilter(request, response);
        }
    }


    private boolean isLoked(String reqPage) {
        if (reqPage.equals("/")) return false;
        for (String page : locked) {
            if (reqPage.contains(page)) {
                return true;
            }
        }
        return false;
    }

    private boolean cookieCheck(HttpServletRequest req) throws IOException, ServletException {
        Cookie[] cookies = req.getCookies();
        if (cookies == null) return false;
        Properties props = new Properties();
        props.load(this.getClass().getClassLoader().getResourceAsStream("users.properties"));
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                if (cookie.getValue().equals(props.getProperty(cookie.getName()))) {
                    req.setAttribute("logPass", cookie.getValue());
                    req.getSession().setAttribute("logged", true);
                    req.getSession().setAttribute("login", cookie.getValue().split(";")[0]);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void destroy() {
    }
}
