package com.getjavajob.phonebook.webapp.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.phonebook.model.Department;
import com.getjavajob.phonebook.model.Employee;
import com.getjavajob.phonebook.model.Phone;
import com.getjavajob.phonebook.sevice.DepartmentService;
import com.getjavajob.phonebook.sevice.EmployeeService;
import com.getjavajob.phonebook.sevice.PhoneService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Set;

@Controller
public class EmpController {

    @Autowired
    private DepartmentService ds;
    @Autowired
    private EmployeeService es;
    @Autowired
    private PhoneService ps;

    @RequestMapping(value = "/")
    public String RdHome() {
        return "redirect:/home";
    }

    @RequestMapping(value = "/home")
    public ModelAndView home() {
        return new ModelAndView("/home");
    }

    @RequestMapping(value = "/ajGetEmp")
    @ResponseBody
    public Employee ajGetEmp(@RequestParam("id") String id) {
        return es.getFromId(Integer.parseInt(id));
    }

    @RequestMapping(value = "/ajAddEmp", method = RequestMethod.POST, produces = {"application/json; charset=UTF-8"})
    @ResponseBody
    public String ajAddEmp(@RequestParam("employeeJSON") String employeeJSON, @RequestParam("dep") String dep,
                           @RequestParam("chief") String chief, @RequestParam("changePic") int changePic, @RequestParam("file") MultipartFile file) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Employee employee = mapper.readValue(employeeJSON, Employee.class);
            if (changePic == 1) {
                if (es.getFromId(employee.getId()) != null) {
                    employee.setPhoto(es.getFromId(employee.getId()).getPhoto());
                } else {
                    employee.setPhoto(null);
                }
            }
            if (changePic == 0) {
                employee.setPhoto(null);
            }
            if (!chief.isEmpty()) {
                employee.setChief(es.getFromId(Integer.parseInt(chief)));
            }
            if (!dep.isEmpty()) {
                employee.setDep(ds.getFromId(Integer.parseInt(dep)));
            }
            if (file.getSize() > 0)
                employee.setPhoto(file.getContentType() + "." + new String(Base64.encodeBase64(file.getBytes())));
            es.add(employee);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "true";
    }

    @RequestMapping(value = "/getViewEmpModal", method = RequestMethod.POST)
    public ModelAndView getViewEmpModal(@RequestParam("id") int id) {
        return getEditOrView(id, "/viewEmpModal");
    }

    @RequestMapping(value = "/getEditEmpModal", method = RequestMethod.POST)
    public ModelAndView getEditEmpModal(@RequestParam("id") int id) {
        return getEditOrView(id, "/editEmpModal");
    }

    private ModelAndView getEditOrView(int id, String url) {
        Set<Employee> employees = es.getAll();
        Set<Department> departments = ds.getAll();
        Set<Phone> phones = ps.getAll();
        Employee employee = es.getFromId(id);
        ModelAndView mav = new ModelAndView(url);
        mav.addObject("employees", employees);
        mav.addObject("employee", employee);
        mav.addObject("phones", phones);
        mav.addObject("birth", es.getSimpleBirth(employee));
        mav.addObject("departments", departments);
        return mav;
    }

    @RequestMapping(value = "/delEmp", method = RequestMethod.POST)
    public void delDep(@RequestParam("id") int id) {
        es.delete(id);
    }

    @RequestMapping(value = "/getAllEmps")
    @ResponseBody
    public Set<Employee> getAllEmps() {
        return es.getAll();
    }

    @RequestMapping(value = "/getEmps")
    @ResponseBody
    public Set<Employee> getEmps(final @RequestParam("filter") String filter, @RequestParam("entity") String entity) {
        Predicate<Employee> predicate;
        final String filterLower = filter.toLowerCase();
        switch (entity) {
            case "name":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getName() != null && employee.getName().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "family":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getFamily() != null && employee.getFamily().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "icq":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getIcq() != null && employee.getIcq().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "mail":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getMail() != null && employee.getMail().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "skype":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getSkype() != null && employee.getSkype().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "dep":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        return employee.getDep() != null && employee.getDep().getName() != null && employee.getDep().getName().toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "phone":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        for (Phone phone : employee.getPhones()) {
                            if (phone.getNumber().contains(filterLower)) {
                                return true;
                            }
                        }
                        return false;
                    }
                };
                break;
            case "addrsH":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        String addrsH = employee.getAddrsH();
                        return addrsH != null && addrsH.toLowerCase().contains(filterLower);
                    }
                };
                break;
            case "addrsW":
                predicate = new Predicate<Employee>() {
                    public boolean evaluate(Employee employee) {
                        String addrsW = employee.getAddrsW();
                        return addrsW != null && addrsW.toLowerCase().contains(filterLower);
                    }
                };
                break;
            default:
                return null;
        }
        Set<Employee> emps = es.getAll();
        CollectionUtils.filter(emps, predicate);
        return emps;
    }
}
