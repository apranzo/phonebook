package com.getjavajob.phonebook.webapp.servlets;

import com.getjavajob.phonebook.sevice.DepartmentService;
import com.getjavajob.phonebook.sevice.EmployeeService;
import com.getjavajob.phonebook.sevice.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Properties;

@Controller
public class LoginController {

    @Autowired
    private DepartmentService ds;
    @Autowired
    private EmployeeService es;
    @Autowired
    private PhoneService ps;

    @RequestMapping(value = "/login")
    public String login(@RequestParam(value = "login", defaultValue = "") String login, @RequestParam(value = "password", defaultValue = "") String pass,
                        HttpServletResponse res, HttpServletRequest req,
                        @RequestParam(value = "remember", required = false) boolean remember) throws IOException {
        String logPass = login + "=" + pass;
        Properties props = new Properties();
        props.load(this.getClass().getClassLoader().getResourceAsStream("users.properties"));
        if (props.getProperty("login").equals(logPass)) {
            req.getSession().setAttribute("logged", true);
            req.getSession().setAttribute("login", login);
            if (remember) {
                Cookie cookie = new Cookie("login", logPass);
                cookie.setMaxAge(60 * 60 * 24 * 365);
                res.addCookie(cookie);
            }
            return "redirect:/";
        } else {
            req.setAttribute("wrongpass", true);
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/ajCheckLogin")
    @ResponseBody
    public String ajCheckLogin(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        return String.valueOf(!((session == null || session.getAttribute("logged") == null && !cookieCheck(req))));
    }


    private boolean cookieCheck(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies == null) return false;
        Properties props = new Properties();
        try {
            props.load(this.getClass().getClassLoader().getResourceAsStream("users.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                if (cookie.getValue().equals(props.getProperty(cookie.getName()))) {
                    req.setAttribute("logPass", cookie.getValue());
                    req.getSession().setAttribute("logged", true);
                    req.getSession().setAttribute("login", cookie.getValue());
                    return true;
                }
            }
        }
        return false;
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest req, HttpServletResponse res) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    cookie.setMaxAge(0);
                    res.addCookie(cookie);
                }
            }
        }
        req.getSession().invalidate();
        return "redirect:/";
    }
}
