package com.getjavajob.phonebook.webapp.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.phonebook.model.Department;
import com.getjavajob.phonebook.model.Employee;
import com.getjavajob.phonebook.sevice.DepartmentService;
import com.getjavajob.phonebook.sevice.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Set;

@Controller
public class DepController {

    @Autowired
    private DepartmentService ds;
    @Autowired
    private EmployeeService es;

    @RequestMapping(value = "/ajAddDep", method = RequestMethod.POST)
    @ResponseBody
    public String ajAddDep(@RequestParam("departmentJSON") String departmentJSON, @RequestParam("head") String head) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Department department = mapper.readValue(departmentJSON, Department.class);
            if (!head.isEmpty()) {
                department.setHead(es.getFromId(Integer.parseInt(head)));
            }
            ds.addOrUpdate(department);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "true";
    }

    @RequestMapping(value = "/delDep", method = RequestMethod.POST)
    public void delDep(@RequestParam("id") int id) {
        ds.delete(id);
    }

    @RequestMapping(value = "/getViewDepModal", method = RequestMethod.POST)
    public ModelAndView getViewEmpModal(@RequestParam("id") int id) {
        return getEditOrView(id, "/viewDepModal");
    }

    @RequestMapping(value = "/getEditDepModal", method = RequestMethod.POST)
    public ModelAndView getEditEmpModal(@RequestParam("id") int id) {
        return getEditOrView(id, "/editDepModal");
    }

    private ModelAndView getEditOrView(int id, String url) {
        Set<Employee> employees = es.getAll();
        Department department = ds.getFromId(id);
        ModelAndView mav = new ModelAndView(url);
        mav.addObject("employees", employees);
        mav.addObject("department", department);
        return mav;
    }

    @RequestMapping(value = "/ajGetDep")
    @ResponseBody
    public Department ajGetDep(@RequestParam("id") String id) {
        return ds.getFromId(Integer.parseInt(id));
    }

    @RequestMapping(value = "/getAllDeps")
    @ResponseBody
    public Set<Department> getAllDeps() {
        return ds.getAll();
    }
}
