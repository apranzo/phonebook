<div class="modal fade" id="viewDepModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Department</h3>
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" id="editButton" data-dismiss="modal"
                                class="btn btn-default btn-hover-green close"
                                onclick="editDepModalInitial('${department.id}')" data-action="save" role="button"
                                hidden>Edit
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form id="viewDep" method="POST">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control en-dis" type="text" name="name" id="name" value="${department.name}"
                               readonly>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="text" name="phone" id="phone"
                               value="${department.phone.number}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="head">Head</label>
                        <input type="text" class="form-control en-dis btn-set-unhid" id="head"
                               value="${department.head.family+" "+department.head.name}" readonly>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal" role="button">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

