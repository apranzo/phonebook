<div class="modal fade" id="editDepModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"></span></button>
                <h3 class="modal-title" id="lineModalLabel">Edit department</h3>
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" data-dismiss="modal" class="btn btn-default btn-hover-green close"
                                onclick="viewDepModalInitial('${department.id}')" role="button">to View
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form id="editDep" method="POST">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control en-dis" type="text" name="name" id="name" value="${department.name}">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="text" name="phone" id="phone"
                               value="${department.phone.number}">
                    </div>
                    <div class="form-group" id="select-employee">
                        <label for="select-emp">Head</label>
                        <select id="select-emp" class="form-control"></select>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group btn-delete" role="group">
                        <button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"
                                onclick="delDep(${department.id})" role="button">Delete
                        </button>
                    </div>
                    <div class="btn-group btn-set-hid" role="group">
                        <button type="button" id="saveImage" class="btn btn-default btn-hover-green"
                                onclick="subAddDep(${department.id})" data-action="save" role="button" hidden>Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

