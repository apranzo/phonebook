<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="viewEmpModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Employee</h3>
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" id="editButton" data-dismiss="modal"
                                class="btn btn-default btn-hover-green close"
                                onclick="editEmpModalInitial('${employee.id}')" data-action="save" role="button" hidden>
                            Edit
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form id="viewEmp" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <img class="img-circle img-responsive img-center" id="userpic"
                             src="${pageContext.request.contextPath}/resources/DefaultAvatar.jpg"
                             style="width: 200px; height: 200px">
                    </div>
                    <div class="form-group">
                        <label for="family">Family</label>
                        <input class="form-control" type="text" name="family" id="family" value="${employee.family}"
                               readonly>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" name="name" id="name" value="${employee.name}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="mail">Email address</label>
                        <input type="email" class="form-control" id="mail" value="${employee.mail}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="icq">icq</label>
                        <input type="text" class="form-control" id="icq" value="${employee.icq}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="skype">Skype</label>
                        <input type="text" class="form-control" id="skype" value="${employee.skype}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="addrsH">Home Address</label>
                        <input type="text" class="form-control" id="addrsH" value="${employee.addrsH}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="addrsW">Work Address</label>
                        <input type="text" class="form-control" id="addrsW" value="${employee.addrsW}" readonly>
                    </div>
                    <div class="form-group" id="select-department">
                        <label for="dep">Department</label>
                        <input type="text" class="form-control" id="dep" value="${employee.dep.name}" readonly>
                    </div>
                    <div class="form-group" id="select-chief">
                        <label for="chief">Chief</label>
                        <input type="text" class="form-control" id="chief" value="${employee.chief}" readonly>
                    </div>
                    <label for="phones">Phones</label>
                    <div class="pFs" id="phones"></div>
                    <div class="form-group registration-date">
                        <label for="birthday">Date:</label>
                        <div class="input-group registration-date-time">
                            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"
                                                                                    aria-hidden="true"></span></span>
                            <input class="form-control en-dis" name="birthday" id="birthday" type="date" readonly>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal" role="button">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
