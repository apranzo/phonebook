<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="editEmpModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"></span></button>
                <h3 class="modal-title" id="lineModalLabel">Employee</h3>
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                        <button type="button" data-dismiss="modal" class="btn btn-default btn-hover-green close"
                                onclick="viewEmpModalInitial('${employee.id}')" role="button">to View
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form id="editEmp" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <img class="img-circle img-responsive img-center" id="userpic"
                             src="${pageContext.request.contextPath}/resources/DefaultAvatar.jpg"
                             style="width: 200px; height: 200px">
                        <input id="file-upload" type="file" name="file-photo" onchange="showUploadedUpic(this)"/>
                        <input id="del-pic" type="button" name="del-photo" onclick="delPic()" value="Удалить"/>
                    </div>
                    <div class="form-group">
                        <label for="family">Family</label>
                        <input class="form-control" type="text" name="family" id="family" value="${employee.family}">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" name="name" id="name" value="${employee.name}">
                    </div>
                    <div class="form-group">
                        <label for="mail">Email address</label>
                        <input type="email" class="form-control" id="mail" value="${employee.mail}">
                    </div>
                    <div class="form-group">
                        <label for="icq">icq</label>
                        <input type="text" class="form-control" id="icq" value="${employee.icq}">
                    </div>
                    <div class="form-group">
                        <label for="skype">Skype</label>
                        <input type="text" class="form-control" id="skype" value="${employee.skype}">
                    </div>
                    <div class="form-group">
                        <label for="addrsH">Home Address</label>
                        <input type="text" class="form-control" id="addrsH" value="${employee.addrsH}">
                    </div>
                    <div class="form-group">
                        <label for="addrsW">Work Address</label>
                        <input type="text" class="form-control" id="addrsW" value="${employee.addrsW}">
                    </div>
                    <div class="form-group" id="select-department">
                        <label for="select-dep">Department</label>
                        <select id="select-dep" class="form-control"></select>
                    </div>
                    <div class="form-group" id="select-employee">
                        <label for="select-emp">Chief</label>
                        <select id="select-emp" class="form-control"></select>
                    </div>
                    <label for="phones">Phones</label>
                    <div class="pFs" id="phones"></div>

                    <div class="form-group">
                        <label for="birthday">Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon"><span class="glyphicon glyphicon-calendar"
                                                                                   aria-hidden="true"></span></span>
                            <input class="form-control" name="birthday" id="birthday" type="date">
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group btn-delete" role="group">
                        <button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"
                                onclick="delEmp(${employee.id})" role="button">Delete
                        </button>
                    </div>
                    <div class="btn-group btn-set-hid" role="group">
                        <button type="button" id="saveImage" class="btn btn-default btn-hover-green"
                                onclick="subAddEmp(${employee.id})" data-action="save" role="button" hidden>Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

