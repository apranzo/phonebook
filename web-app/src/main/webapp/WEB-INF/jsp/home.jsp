<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/basic.css" rel="stylesheet">
    <title>PBook</title>

</head>
<body>
<div id="container" class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">PhoneBook</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href=# onclick="addEmpModalInitial()">Add Employee</a></li>
                        <li><a href=# onclick="addDepModalInitial()">Add Department</a></li>
                        <li>
                            <jsp:include page="${pageContext.request.contextPath}authoris.jsp"/>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="container-fluid">
        <div class="row">
            <form class="search-form" onsubmit="getSearchResult();return false">
                <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control" name="query" id="query" placeholder="Select filter"
                                   disabled>
                    <span class="input-group-addon">
                        <button type="submit" id="search-button" disabled>
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                <div class="btn-group btn-group-justified selector-buttons">
                    <div class="btn-group">
                        <button type="button" id="family_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="family">family
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="name_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="name">name
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="mail_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type " value="mail">e-mail
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="icq_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="icq">icq
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="department_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="dep">department
                        </button>
                </div>
                    <div class="btn-group">
                        <button type="button" id="phone_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="phone">phone
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="skype_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="skype">skype
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="addrsH_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="addrsH">home
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="addrsW_button" onclick="radiolizing(this)"
                                class="btn btn-default search-type" value="addrsW">work
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="search-result col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8"
                 id="searchDiv">
            </div>
        </div>
    </div>
</div>

<div class="modal-from-str" id="modal"></div>

<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/modal.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script>
    var context = "${pageContext.request.contextPath}";
</script>
</body>
</html>
