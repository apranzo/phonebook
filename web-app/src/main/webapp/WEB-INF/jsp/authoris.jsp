<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <c:when test="${pageContext.session.getAttribute('logged').equals(true)}">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">${pageContext.request.getSession(false).getAttribute("login")}
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li role="separator" class="divider"></li>
                <li><a href="logout">Выйти</a></li>
            </ul>
        </li>
    </c:when>
    <c:when test="${!pageContext.session.getAttribute('logged').equals(true)}">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">Log in
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <form action="login" method="post">
                        <label for="login-field">Ваш логин</label>
                        <input class="form-control" type="text" name="login" id="login-field">
                        <label for="password-field">Ваш пароль</label>
                        <input class="form-control" type="password" name="password" id="password-field">
                        <input class="form-control" type="submit" value="Войти">
                        <label><input type="checkbox" name="remember"> Запомнить меня</label>
                    </form>
                </li>
            </ul>
        </li>
    </c:when>
</c:choose>
