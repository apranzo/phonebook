var phons = [];

var searchType;

var changePic = 1;

var pFCount = 1;
function validatePhone(phone) {
    return phone.length > 3;
}

function delPic() {
    if (confirm("Вы уверены?")) {
        $('#userpic').attr('src', context + "/resources/DefaultAvatar.jpg");
        changePic = 0;
        document.getElementById('file-upload').innerHTML = document.getElementById('file-upload').innerHTML;
    }
}

function showUploadedUpic(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#userpic').attr('src', e.target.result);
            changePic = 2
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function delEmp(id) {
    if (confirm("Вы уверены?")) {
        $.ajax({
            url: context + "/delEmp",
            type: "POST",
            data: {id: id},
            success: function () {
                alert("Employee deleted");
                window.location.replace(context + "/home");
            }
        });
    }
}
function delDep(id) {
    if (confirm("Вы уверены?")) {
        $.ajax({
            url: context + "/delDep",
            type: "POST",
            data: {id: id},
            success: function () {
                alert("Department deleted");
                window.location.replace(context + "/home");
            }
        });
    }
}

function addOrDellPhone(id) {
    var buttonElement = document.getElementById('bPF' + id);
    var pF = document.getElementById("iPF" + id);
    var phoneType = document.getElementById("rHPF" + id);
    if (buttonElement.textContent == '+') {
        var number = pF.value;
        var type;
        if (phoneType.checked) {
            type = 'HOME'
        } else {
            type = 'WORK'
        }
        phons.push({number: number, type: type});
        addPhoneField();
        pF.readOnly = true;
        phoneType.disabled = true;
        document.getElementById("rPF" + id).disabled = true;
        pF.style.background = 'beige';
        pF.style.fontWeight = "bold";
    } else {
        delPhoneField(id);
    }
}

function addPhoneField() {
    ++pFCount;
    var newFieldId = "\'" + "pF" + pFCount + "\'";
    var buttonElement = document.getElementById("bPF" + (pFCount - 1));
    var div = '<div id=' + newFieldId + '>';
    var input = '<input autocomplete="off"  class="form-control" id="iPF' + pFCount + '" name="phoneInput" type="text" placeholder="Only numbers"/>';
    var button = '<button id="bPF' + pFCount + '" name="plusMinusPhone" onclick="addOrDellPhone(' + pFCount + ')" type="button">+</button>';
    var radio = '<label>Work<input type="radio" name="rPF' + pFCount + '" id="rPF' + pFCount + '" autocomplete="on" checked></label>' +
        '<label>Home<input type="radio" name="RPF' + pFCount + '" id="rHPF' + pFCount + '" autocomplete="off"></label>';
    $(buttonElement.parentNode.parentNode).append(div + "\n" + input + "\n" + radio + "\n" + button + "</div>");
    buttonElement.textContent = '-';
}

function delPhoneField(id) {
    var div = document.getElementById("pF" + id);
    var number = document.getElementById("iPF" + id).value;
    var phoneType = document.getElementById("rHPF" + id);
    var type;
    if (phoneType.checked) {
        type = 'HOME'
    } else {
        type = 'WORK'
    }
    div.remove();
    var index = -1;
    for (var i = 0; i < phons.length; i++) {
        if (JSON.stringify(phons[i]) == JSON.stringify({number: number, type: type})) {
            index = i;
            break;
        }
    }
    phons.splice(index, 1);
}

function subAddEmp(id) {
    var approved = confirm("Вы уверенны?");
    if (!approved) {
        return false;
    }
    var employee = {
        id: id,
        family: document.getElementById("family").value,
        name: document.getElementById("name").value,
        mail: document.getElementById("mail").value,
        icq: document.getElementById("icq").value,
        skype: document.getElementById("skype").value,
        phones: phons,
        birth: document.getElementById("birthday").value,
        addrsH: document.getElementById("addrsH").value,
        addrsW: document.getElementById("addrsW").value
    };
    var files = document.getElementById("file-upload").files;
    var formData = new FormData();
    var file = new File([""], "file");
    if (changePic == 2 && files && files.length > 0) {
        file = files[0];
    }
    formData.append("file", file);
    formData.append("changePic", changePic);
    formData.append("dep", document.getElementById("select-dep").value);
    formData.append("chief", document.getElementById("select-emp").value);
    formData.append("employeeJSON", JSON.stringify(employee));
    $.ajax({
        url: context + "/ajAddEmp",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            alert("is added: " + data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    changePic = 1;
}

function subAddDep(id) {
    var approved = confirm("Вы уверенны?");
    if (!approved) {
        return false;
    }
    var department = {
        id: id,
        name: document.getElementById("name").value,
        phone: {
            number: document.getElementById("phone").value,
            type: "WORK"
        }
    };
    $.ajax({
        url: context + "/ajAddDep",
        type: "POST",
        data: {
            departmentJSON: JSON.stringify(department),
            head: document.getElementById("select-emp").value
        },
        success: function (data) {
            alert("is added: " + data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + errorThrown + document.getElementById("head").value);

        }
    });
}

$(function () {
    $('[checked="checked"]').addClass('active')
});

function radiolizing(button) {
    searchType = button.value;
    var buttons = document.getElementsByClassName("search-type");
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("active");
    }
    button.classList.add("active");
    document.getElementById("query").placeholder = "Search";
    document.getElementById("query").disabled = false;
    document.getElementById("search-button").disabled = false;
}

function selectDep(selected) {
    $.ajax({
        url: context + "/getAllDeps",
        success: function (data) {
            var select = document.getElementById("select-dep");
            var sel = '';
            $(select).append("<option value=''></option>");
            for (var i = 0; i < data.length; i++) {
                if (data[i].id == selected) {
                    sel = 'selected="selected"';
                }
                $(select).append("<option " + sel + " value=" + data[i].id + ">" + data[i].name + "</option>");
                sel = '';
            }
        }
    });
}

function selectEmp(selected) {
    $.ajax({
        url: context + "/getAllEmps",
        success: function (data) {
            var select = document.getElementById("select-emp");
            var sel = '';
            $(select).append("<option value=''></option>");
            for (var i = 0; i < data.length; i++) {
                if (data[i].id == selected) {
                    sel = 'selected="selected"';
                }
                $(select).append("<option value=" + data[i].id + ">" + data[i].name + data[i].family + "</option>");
                sel = '';
            }
        }
    });
}

function phonesForEdit(phones) {
    var phones_ed = document.getElementById("phones");
    while (phones_ed.firstChild) {
        phones_ed.removeChild(phones_ed.firstChild);
    }
    for (var i = 0; i < phones.length; i++) {
        pFCount++;
        var workTrue = "";
        var homeTrue = "";
        if (phones[i].type == "HOME") {
            homeTrue = ' checked';
        } else {
            workTrue = ' checked';
        }
        $(phones_ed).append('<div id=pF' + pFCount + '>' +
            '<input autocomplete="off"  class="form-control" id=iPF' + pFCount + ' name="phoneInput" type="text" readonly/>' +
            '<label>Work<input type="radio" name=rPF' + pFCount + ' id=rPF' + pFCount + '' + workTrue + ' disabled></label>' +
            '<label>Home<input type="radio" name=rPF' + pFCount + ' id=rHPF' + pFCount + '' + homeTrue + ' disabled></label>' +
            '<button class="set-hid" id=bPF' + pFCount + ' name="plusMinusPhone" onclick="addOrDellPhone(' + pFCount + ')" type="button">-</button><br>' +
            '</div>');
        document.getElementById("iPF" + pFCount).setAttribute("value", phones[i].number);
    }
    pFCount++;
    $(phones_ed).append('<div class="set" id=pF' + pFCount + '>' +
        '<input autocomplete="off"  class="form-control" id=iPF' + pFCount + ' name="phoneInput" type="text"/>' +
        '<label>Work<input class="" type="radio" name=rPF' + pFCount + ' id=rPF' + pFCount + ' autocomplete="on" checked></label>' +
        '<label>Home<input class="" type="radio" name=rPF' + pFCount + ' id=rHPF' + pFCount + '></label>' +
        '<button class="en-dis" id=bPF' + pFCount + ' name="plusMinusPhone" onclick="addOrDellPhone(' + pFCount + ')" type="button">+</button><br>' +
        '</div>');
    return phones_ed.innerHTML;
}

function phonesForView(phones) {
    var phones_ed = document.getElementById("phones");
    while (phones_ed.firstChild) {
        phones_ed.removeChild(phones_ed.firstChild);
    }
    for (var i = 0; i < phones.length; i++) {
        pFCount++;
        var workTrue = "";
        var homeTrue = "";
        if (phones[i].type == "HOME") {
            homeTrue = ' checked';
        } else {
            workTrue = ' checked';
        }
        $(phones_ed).append('<div id=pF' + pFCount + '>' +
            '<input autocomplete="off"  class="form-control" id=iPF' + pFCount + ' name="phoneInput" type="text" readonly/>' +
            '<label>Work<input type="radio" name=rPF' + pFCount + ' id=rPF' + pFCount + '' + workTrue + ' disabled></label>' +
            '<label>Home<input type="radio" name=rPF' + pFCount + ' id=rHPF' + pFCount + '' + homeTrue + ' disabled></label>' +
            '</div>');
        document.getElementById("iPF" + pFCount).setAttribute("value", phones[i].number);
    }
    return phones_ed.innerHTML;
}

function getSearchResult() {
    var table = document.getElementById("searchResult");
    if (table != null) {
        table.remove();
    }
    $("#searchDiv").append("<table class='table table-hover' id='searchResult'>" +
        "<thead>" +
        "<tr>" +
        "<th>#</th>" +
        "<th>family</th>" +
        "<th>name</th>" +
        "<th>department</th>" +
        "<th>e-mail</th>" +
        "<th>icq</th>" +
        "<th>skype</th>" +
        "<th style='width: 36px;'></th>" +
        "</tr>" +
        "</thead>" +
        "</table>");
    var tbody = document.createElement("tbody");
    $.ajax({
        url: context + "/getEmps",
        data: {
            filter: function () {
                return document.getElementById("query").value;
            },
            entity: searchType
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var tr = document.createElement("tr");
                var emp = data[i];
                var num = document.createElement("th");
                num.textContent = i + 1;
                tr.appendChild(num);
                var family = document.createElement("th");
                var name = document.createElement("th");
                var dep = document.createElement("th");
                var mail = document.createElement("th");
                var icq = document.createElement("th");
                var skype = document.createElement("th");
                var buttons = document.createElement("th");
                family.textContent = emp.family;
                name.textContent = emp.name;
                if (emp.dep != null) {
                    dep.innerHTML = '<a href=# onclick="viewDepModalInitial(' + emp.dep.id + ')">' +
                        emp.dep.name + '</a>';
                }
                mail.textContent = emp.mail;
                icq.textContent = emp.icq;
                skype.textContent = emp.skype;
                var controler = "'getViewEmpModal'";
                buttons.innerHTML = '<a href=# onclick="viewEmpModalInitial(' + emp.id + ',' + controler + ')">' +
                    '<span class="glyphicon glyphicon-eye-open"/> </a>';
                tr.appendChild(family);
                tr.appendChild(name);
                tr.appendChild(dep);
                tr.appendChild(mail);
                tr.appendChild(icq);
                tr.appendChild(skype);
                tr.appendChild(buttons);
                tbody.appendChild(tr);
            }
            document.getElementById("searchResult").appendChild(tbody);
        }
    });
    return false;
}

$('.btn-success>input').click(function () {
    alert('test');
    $('input[type="radio"][name="options"]').not(':checked').prop("checked", true);
});

$(document).ready(function () {
    $("#query").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: context + "/getEmps",
                data: {
                    filter: request.term,
                    entity: searchType
                },
                success: function (data) {
                    response($.map(data, function (employee, i) {
                        return {
                            value: eval("employee." + searchType),
                            label: employee.name + " " + employee.family
                        }
                    }));
                }
            })
        },
        minLength: 2
    });
});