function viewLogin() {
    $("#loginModal").modal("hide");
    $(".modal-from-str").load(context + "/resources/html/login.html", function () {
        $("#loginModal").modal("show");
    });
}

function checkLogin() {
    return $.ajax({
        url: context + "/ajCheckLogin",
        type: "POST",
        async: false
    }).responseText;
}

function viewDepModalInitial(id) {
    pFCount = 1;
    var phones = [];
    $.ajax({
        url: context + "/getViewDepModal",
        type: "POST",
        data: {id: id},
        async: false,
        success: function (responseText) {
            document.getElementById("modal").innerHTML = responseText;
            $("#viewDepModal").modal("show");
        }
    });
}
function editDepModalInitial(id) {
    pFCount = 1;
    var phones = [];
    if (checkLogin() == "true") {
        $.ajax({
            url: context + "/getEditDepModal",
            type: "POST",
            data: {id: id},
            async: false,
            success: function (responseText) {
                document.getElementById("modal").innerHTML = responseText;
                var head = undefined;
                var department = getDep(id);
                if (department.head != null) {
                    head = department.head.id;
                }
                selectEmp(head);
                $("#editDepModal").modal("show");
            }
        });
    } else viewLogin();
}

function getDep(id) {
    return $.ajax({
        url: context + "/ajGetEmp",
        type: "POST",
        data: {id: id},
        async: false,
        success: function (employee) {
            return employee;
        }
    });
}


function viewEmpModalInitial(id) {
    pFCount = 1;
    var phones = [];
    $("#editEmpModal").modal("hide");
    $.ajax({
        url: context + "/getViewEmpModal",
        type: "POST",
        data: {id: id},
        async: false,
        success: function (responseText) {
            document.getElementById("modal").innerHTML = responseText;
            $.ajax({
                url: context + "/ajGetEmp",
                type: "POST",
                data: {id: id},
                async: false,
                success: function (employee) {
                    if (employee.photo != null) {
                        var sep = employee.photo.indexOf(".");
                        var photo = employee.photo.substr(sep + 1, employee.photo.length);
                        var reader = new FileReader();
                        avatar = document.getElementById('userpic');
                        avatar.setAttribute('src', 'data:' + photo.substr(0, sep) + ';base64, ' + photo);
                    }
                    var dep = document.getElementById("dep");
                    var chief = document.getElementById("chief");
                    var phones = document.getElementById("phones");
                    var birthday = document.getElementById("birthday");
                    if (employee.chief != null) {
                        chief.value = employee.chief.name + " " + employee.chief.family;
                    }
                    phones.innerHTML = phonesForView(employee.phones);
                    if (employee.birth != null) {
                        date = new Date(employee.birth);
                        birthday.defaultValue = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    }
                    $("#viewEmpModal").modal("show");
                }
            });
        }
    });
}

function editEmpModalInitial(id) {
    pFCount = 1;
    var phones = [];
    if (checkLogin() == "true") {
        $("#editEmpModal").modal("hide");
        $.ajax({
            url: context + "/getEditEmpModal",
            type: "POST",
            data: {id: id},
            async: false,
            success: function (responseText) {
                document.getElementById("modal").innerHTML = responseText;
                $.ajax({
                    url: context + "/ajGetEmp",
                    type: "POST",
                    async: false,
                    data: {id: id},
                    success: function (employee) {
                        if (employee.photo != null) {
                            var sep = employee.photo.indexOf(".");
                            var photo = employee.photo.substr(sep + 1, employee.photo.length);
                            var reader = new FileReader();
                            avatar = document.getElementById('userpic');
                            avatar.setAttribute('src', 'data:' + photo.substr(0, sep) + ';base64, ' + photo);
                        }
                        var dep = document.getElementById("dep");
                        var chief = document.getElementById("chief");
                        var phones = document.getElementById("phones");
                        var birthday = document.getElementById("birthday");
                        var depId = undefined;
                        var chiefId = undefined;
                        if (employee.dep != null) {
                            depId = employee.dep.id;
                        }
                        if (employee.chief != null) {
                            chiefId = employee.chief.id;
                        }
                        selectDep(depId);
                        selectEmp(chiefId);
                        phones.innerHTML = phonesForEdit(employee.phones);
                        if (employee.birth != null) {
                            date = new Date(employee.birth);
                            birthday.defaultValue = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                        }
                        $("#editEmpModal").modal("show");
                    }
                });
            }
        });
    } else viewLogin();

}


function addDepModalInitial() {
    pFCount = 1;
    var phones = [];
    if (checkLogin() == "true") {
        $("#addDepModal").modal("hide");
        $(".modal-from-str").load(context + "/resources/html/addDepModal.html", function () {
            $.ajax({
                url: context + "/getAllEmps",
                async: false,
                success: function (data) {
                    var allEmployees = data;
                    var select = document.getElementById("select-emp");
                    $(select).append("<option value=''></option>");
                    for (var i = 0; i < allEmployees.length; i++) {
                        $(select).append("<option value=" + allEmployees[i].id + ">" + allEmployees[i].name + " " + allEmployees[i].family + "</option>");
                    }
                }
            });
            $("#addDepModal").modal("show");
        });
    } else viewLogin();
}

function addEmpModalInitial() {
    pFCount = 1;
    var phones = [];
    if (checkLogin() == "true") {
        $("#addEmpModal").modal("hide");
        $(".modal-from-str").load(context + "/resources/html/addEmpModal.html", function () {
            $.ajax({
                url: context + "/getAllDeps",
                async: false,
                success: function (allDepartmens) {
                    var select = document.getElementById("select-dep");
                    $(select).append("<option value=''></option>");
                    for (var i = 0; i < allDepartmens.length; i++) {
                        $(select).append("<option value=" + allDepartmens[i].id + ">" + allDepartmens[i].name + "</option>");
                    }
                }
            });
            $.ajax({
                url: context + "/getAllEmps",
                async: false,
                success: function (allEmployees) {
                    var select = document.getElementById("select-emp");
                    $(select).append("<option value=''></option>");
                    for (var i = 0; i < allEmployees.length; i++) {
                        $(select).append("<option value=" + allEmployees[i].id + ">" + allEmployees[i].name + " " + allEmployees[i].family + "</option>");
                    }
                }
            });
            $('#userpic').attr('src', context + "/resources/DefaultAvatar.jpg");
            $("#addEmpModal").modal("show");
        });
    } else {
        viewLogin();
    }
}